import sys
import design
import sql_Queries
import sql_req
from PyQt5 import QtWidgets
from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator


class App(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.toolBox.currentChanged.connect(self.load_db)

        self.add_director_btn.clicked.connect(self.add_director)
        self.add_studio_btn.clicked.connect(self.add_studio)
        self.add_film_btn.clicked.connect(self.add_film)
        self.add_viewer_btn.clicked.connect(self.add_viewer)
        self.add_cinema_btn.clicked.connect(self.add_cinema)
        self.add_session_btn.clicked.connect(self.add_session)
        self.add_viewer_to_session_btn.clicked.connect(self.add_viewer_to_session)
        self.find_directors_btn.clicked.connect(self.find_directors)
        self.find_sessions_btn.clicked.connect(self.find_sessions)
        self.find_studios_btn.clicked.connect(self.find_studios)

        validator = QRegExpValidator(QRegExp('[a-zA-Zа-яА-я ]+'), self)
        address_validator = QRegExpValidator(QRegExp('[a-zA-Zа-яА-я0-9 ,.]+'), self)
        self.director_name.setValidator(validator)
        self.studio_name.setValidator(validator)
        self.film_name.setValidator(validator)
        self.film_genre.setValidator(validator)
        self.viewer_name.setValidator(validator)
        self.cinema_name.setValidator(validator)
        self.cinema_address.setValidator(address_validator)
        self.find_directors_genre.setValidator(validator)
        self.find_sessions_film.setValidator(validator)
        self.find_studios_viewer.setValidator(validator)


    def load_db(self):
        self.film_studio.clear()
        for studio in sql_req.get_studios():
            self.film_studio.addItem(studio.title, studio.id)
        self.film_director.clear()
        for director in sql_req.get_directors():
            self.film_director.addItem(director.name, director.id)
        self.session_film.clear()
        for film in sql_req.get_films():
            self.session_film.addItem(film.title, film.id)
        self.session_cinema.clear()
        for cinema in sql_req.get_cinemas():
            self.session_cinema.addItem(cinema.title, cinema.id)
        self.viewertosession.clear()
        for viewer in sql_req.get_viewers():
            self.viewertosession.addItem(viewer.name, viewer.id)
        self.sessiontoviewer.clear()
        for session in sql_req.get_sessions():
            self.sessiontoviewer.addItem(session['title'], session['id'])


    def add_director(self):
        if self.director_name.text():
            name = self.director_name.text()
            age = self.director_age.text()
            self.director_name.clear()
            sql_req.add_director(name, age)


    def add_studio(self):
        if self.studio_name.text():
            name = self.studio_name.text()
            budget = self.studio_budget.text()
            self.studio_name.clear()
            sql_req.add_studio(name, budget)


    def add_film(self):
        if self.film_name.text() and self.film_genre.text():
            name = self.film_name.text()
            genre = self.film_genre.text()
            self.film_name.clear()
            self.film_genre.clear()
            studio = self.film_studio.currentData()
            director = self.film_director.currentData()
            sql_req.add_film(name, genre, studio, director)


    def add_viewer(self):
        if self.viewer_name.text():
            name = self.viewer_name.text()
            self.viewer_name.clear()
            sql_req.add_viewer(name)


    def add_cinema(self):
        if self.cinema_name.text() and self.cinema_address.text():
            name = self.cinema_name.text()
            address = self.cinema_address.text()
            self.cinema_name.clear()
            self.cinema_address.clear()
            sql_req.add_cinema(name, address)


    def add_session(self):
        time = self.session_time.dateTime().toPyDateTime()
        duration = self.session_duration.time().toPyTime()
        film = self.session_film.currentData()
        cinema = self.session_cinema.currentData()
        sql_req.add_session(time, duration, film, cinema)


    def add_viewer_to_session(self):
        viewer = self.viewertosession.currentData()
        session = self.sessiontoviewer.currentData()
        print(viewer, session)
        sql_req.add_view_sess(viewer, session)


    def find_directors(self):
        if self.find_directors_genre.text():
            genre = self.find_directors_genre.text()
            self.find_directors_genre.clear()
            result = sql_Queries.find_directors(genre)
            self.find_directors_result.setText(result)


    def find_sessions(self):
        if self.find_sessions_film.text():
            title = self.find_sessions_film.text()
            self.find_sessions_film.clear()
            result = sql_Queries.find_sessions(title)
            self.find_sessions_result.setText(result)


    def find_studios(self):
        if self.find_studios_viewer.text():
            time = self.find_studios_time.dateTime().toPyDateTime()
            viewer = self.find_studios_viewer.text()
            self.find_studios_viewer.clear()
            result = sql_Queries.find_studios(time, viewer)
            self.find_studios_result.setText(result)


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = App()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
