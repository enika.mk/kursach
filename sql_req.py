from peewee import *
from datetime import datetime, time
from models import Director, Studio, Session, Film, Viewer, Cinema, ViewerToSession
import peewee


def add_director(name, age):
    return Director.create(name=name, age=age)


def get_directors():
    return Director.select(Director.name, Director.id)


def add_studio(title, budget):
    return Studio.create(title=title, budget=budget)


def get_studios():
    return Studio.select(Studio.title, Studio.id)


def add_session(time, duration, film, cinema):
    return Session.create(time=time, duration=duration, film=film, cinema=cinema)


def get_sessions():
    result = list()
    for session in Session.select(
        Session.id,
        Film.title.alias('film_title'),
        Cinema.title.alias('cinema_title'),
        Session.time
        ).join(Cinema).switch().join(Film).tuples():
        result.append({
            'id': session[0],
            'title': "{} - {} - {}".format(session[1], session[2], session[3])
        })
    return result

def add_film(title, genre, studio, director):
    return Film.create(title=title, genre=genre, studio=studio, director=director)


def get_films():
    return Film.select(Film.title, Film.id)


def add_viewer(name):
    return Viewer.create(name=name)


def get_viewers():
    return Viewer.select(Viewer.id, Viewer.name)


def add_cinema(title, address):
    return Cinema.create(title=title, address=address)


def get_cinemas():
    return Cinema.select(Cinema.title, Cinema.id)


def add_view_sess(viewer, session):
    return ViewerToSession.create(viewer=viewer, session=session)


if __name__ == '__main__':
    director1 = add_director(name='Peter Farrelly', age=63)
    studio1 = add_studio(title='Universal Pictures', budget=23000000)
    film1 = add_film(title='Green Book', genre='biographical comedy-drama', studio=studio1, director=director1)
    cinema1 = add_cinema(title='Cinema Village', address='22 E 12th St, New York')
    session1 = add_session(time=datetime(2019, 1, 24, 15, 45), duration=time(2, 10), film=film1, cinema=cinema1)
    viewer1 = add_viewer(name='Peter Johnson')
    view_sess1 = add_view_sess(viewer=viewer1, session=session1)
    director2 = add_director(name= 'Guy Ritchie', age=51)
    studio2 = add_studio(title= 'Miramax', budget=22000000)
    film2 = add_film(title='The Gentlemen', genre='action&comedy', studio=studio2, director=director2)
    cinema2 = add_cinema(title= 'BFI IMAX', address = '1 Charlie Chaplin Walk, South Bank, London')
    session2 = add_session(time=datetime(2020, 2, 13, 18, 30), duration=time(1, 53), film=film2, cinema=cinema2)
    viewer2 = add_viewer(name = 'Jason Smith')
    view_sess2 = add_view_sess(viewer=viewer2, session=session2)
    director3 = add_director(name='Todd Phillips', age=49)
    studio3 = add_studio(title='Warner Bros. Pictures', budget=62500000)
    film3 = add_film(title='Joker', genre='psychological thriller', studio=studio3, director=director3)
    cinema3 = add_cinema(title='Karo-Premier', address=' 24, New Arbat, Moscow')
    session3 = add_session(time=datetime(2019, 10, 4, 17, 20), duration=time(2, 2), film=film3, cinema=cinema3)
    viewer3 = add_viewer(name='Waene Wilslow')
    view_sess3 = add_view_sess(viewer=viewer3, session=session3)
    director4 = add_director(name='Tim Miller', age=55)
    studio4 = add_studio(title='20th Century Fox', budget=58000000)
    film4 = add_film(title='Deadpool', genre='action&comedy', studio=studio4, director=director4)
    cinema4 = add_cinema(title='Quad Cinema', address='34 W 13th St, New York')
    session4 = add_session(time=datetime(2016, 2, 8, 21, 50), duration=time(1, 49), film=film4, cinema=cinema4)
    viewer4 = add_viewer(name='T.J. Kampor')
    view_sess4 = add_view_sess(viewer=viewer4, session=session4)
