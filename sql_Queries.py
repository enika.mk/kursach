from peewee import *
from models import Director, Studio, Session, Film, Viewer, Cinema, ViewerToSession
import peewee
import datetime


def find_directors(genre):
    result = list()
    for director in (Director.select()
        .join(Film)
        .where(Film.genre == genre)):
        result.append(director.name)
    return '\n'.join(result)


def find_sessions(film):
    result = list()
    for session in (Session.select(
        Cinema.title,
        Cinema.address,
        Session.time)
        .join(Cinema)
        .switch()
        .join(Film)
        .where(Film.title == film)
        .order_by(Session.time.desc())
        .tuples()):
        session = list(session)
        session[2] = str(session[2])
        result.append(' - '.join(session))
    return '\n'.join(result)


def find_studios(time, viewer):
    result = list()
    for studio in (Studio.select(
        Studio.title,
        Studio.budget)
        .join(Film)
        .join(Session)
        .join(ViewerToSession)
        .join(Viewer)
        .where(Session.time > time, Viewer.name == viewer)):
        result.append('{} - {}'.format(studio.title, studio.budget))
    return '\n'.join(result)


if __name__ == '__main__':
    print(find_directors('action&comedy'))
    print(find_sessions('Deadpool'))
    print(find_studios(datetime.datetime(2016, 2, 8), 'Waene Wilslow'))
