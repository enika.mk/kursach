from peewee import *
from datetime import datetime

db = SqliteDatabase('cinema.db', pragmas={'foreign_keys': 1})

class BaseModel(Model):
    class Meta:
        database = db

class Director(BaseModel):
    name = CharField(default='John')
    age = IntegerField(default=30)

class Studio(BaseModel):
	title = CharField()
	budget = IntegerField()

class Cinema(BaseModel):
	title = CharField()
	address = CharField()

class Film(BaseModel):
	title = CharField()
	genre = CharField()
	director = ForeignKeyField(Director, backref='films')
	studio = ForeignKeyField(Studio, backref='films')

class Session(BaseModel):
	time = DateTimeField(default=datetime.now())
	duration = TimeField()
	film = ForeignKeyField(Film, backref='sessions')
	cinema = ForeignKeyField(Cinema, backref='sessions')

class Viewer(BaseModel):
	name = CharField()

class ViewerToSession(BaseModel):
    viewer = ForeignKeyField(Viewer, backref='viewer_sessions')
    session = ForeignKeyField(Session, backref='session_viewers')

    class Meta:
        primary_key = CompositeKey('viewer', 'session')


if __name__ == '__main__':
	Director.create_table()
	Film.create_table()
	Studio.create_table()
	Session.create_table()
	Viewer.create_table()
	Cinema.create_table()
	ViewerToSession.create_table()
	